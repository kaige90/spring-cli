package priv.chenkai.spring_cli.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import priv.chenkai.spring_cli.vo.UserVo;

/**
 * Created by chenkai on 2018/1/9.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class UserServiceTest {
    @Autowired
    private UserService userService;

    /**
     * 添加用户测试
     */
    @Test
    public void addTest(){
        UserVo vo = new UserVo();
        vo.setName("张晓燕");
        vo.setMobile("12610231231");
        vo.setNickname("小燕子");
        vo.setSex(0);
        userService.insert(vo);
    }

    @Test
    public void updateTest(){
        UserVo vo = new UserVo();
        vo.setId(1);
        vo.setNickname("小猪佩琦");
        userService.update(vo);
    }

}
