package priv.chenkai.spring_cli.common.log;

import java.lang.annotation.*;

/**
 *
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpLog {
    /**
     * 操作类型描述
     * @return
     */
    String operateTypeDesc() default "";

    String id() default "-1";

    /**
     * 操作类型
     * @return
     */
    enum OpType{ ADD,UPDATE, DEL, SEARCH};

    OpType type() default OpType.SEARCH;

    /**
     * 模块编码
     * @return
     */
    String moudleCode() default "M30";

    /**
     * 模块名称
     * @return
     */
    String moudleName() default "XX模块";

    /**
     * 业务类型
     * @return
     */
    String bussType() default "";
    /**
     * 业务类型描述
     * @return
     */
    String bussTypeDesc() default "";
}
