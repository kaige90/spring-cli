package priv.chenkai.spring_cli.server;

import org.springframework.beans.factory.annotation.Autowired;
import priv.chenkai.spring_cli.client.UploadAPI;
import priv.chenkai.spring_cli.client.bo.FileBo;
import priv.chenkai.spring_cli.service.FileUploadService;

import java.io.IOException;
import java.util.List;

/**
 * @Author:chenkai
 * @Date: 2018/5/11 11:39
 */
public class UploadAPIImpl implements UploadAPI{
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    public String upload(byte[] file,String contentType, String rootPath) throws IOException {
        return fileUploadService.upload(file,contentType,rootPath);
    }

    @Override
    public List<String> uploads(List<FileBo> files, String rootPath) throws IOException {
        return fileUploadService.uploads(files,rootPath);
    }
}
