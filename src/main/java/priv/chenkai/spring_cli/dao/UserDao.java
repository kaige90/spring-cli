package priv.chenkai.spring_cli.dao;

import org.apache.ibatis.annotations.Param;
import priv.chenkai.spring_cli.model.User;

import java.util.List;

/**
 * Created by chenkai on 2018/1/8.
 */
public interface UserDao {
    List<User> find(@Param("start") Integer start, @Param("limit") Integer limit);

    int count();

    int insert(User user);

    int update(User user);

    int delete(@Param("id") Integer id);

    User findById(@Param("id") Integer id);
}
