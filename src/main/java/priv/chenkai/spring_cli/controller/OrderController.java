package priv.chenkai.spring_cli.controller;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import priv.chenkai.spring_cli.common.log.OpLog;
import priv.chenkai.spring_cli.util.Result;
import priv.chenkai.spring_cli.vo.OrderVo;

import java.util.UUID;


@RestController
public class OrderController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 查询订单列表
     * @param start
     * @param size
     * @return
     */
    @OpLog(type = OpLog.OpType.SEARCH,id = "10010")
    @GetMapping("/api/orders")
    public Result list(Integer start, Integer size){
        return Result.successResult(Lists.newArrayList(1,2,5,7,8,10));
    }


    /**
     * 添加订单
     * @param orderVo
     * @return
     */
    @OpLog(type = OpLog.OpType.ADD, id = "10011")
    @PostMapping("/api/orders")
    public Result add(@RequestBody OrderVo orderVo){
        orderVo.setOrderId(UUID.randomUUID().toString());
        return Result.successResult(orderVo);
    }


}
