package priv.chenkai.spring_cli.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import priv.chenkai.spring_cli.exception.ReadMessageException;
import priv.chenkai.spring_cli.util.Result;

import static org.springframework.http.HttpStatus.OK;


/**
 * 全局异常拦截
 * @author: chenkai
 * @since: 2017/7/19.
 */
@ControllerAdvice
public class GlobalExceptionController extends ResponseEntityExceptionHandler{
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 通用异常，打印错误栈
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class,NullPointerException.class})
    @ResponseBody
    public Result runtimeExceptionHandler(Exception e){
        Result result = new Result();
        result.setFalse();
        result.setMessage("系统异常，请稍后再试～");
        logger.error(e.getLocalizedMessage(),e);
        //TODO 发邮件
        return result;
    }

    /**
     * 只读异常
     * @param e
     * @return
     */
    @ExceptionHandler({IllegalArgumentException.class, ReadMessageException.class})
    @ResponseBody
    public Result readMessageException(Exception e){
        Result result = new Result();
        result.setFalse();
        result.setMessage(e.getMessage());
        return result;
    }

    /**
     * 执行RequestMapping的异常，参数错误，方法不对等
     * @param ex
     * @param body
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Result result = new Result();
        result.setFalse();
        result.setMessage(ex.getMessage());
        return new ResponseEntity<>(result, OK);
    }
}
