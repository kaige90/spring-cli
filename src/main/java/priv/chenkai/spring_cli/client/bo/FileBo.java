package priv.chenkai.spring_cli.client.bo;

import java.io.Serializable;

/**
 * 文件对象
 * @Author:chenkai
 * @Date: 2018/5/11 11:07
 */
public class FileBo implements Serializable{
    private static final long serialVersionUID = 1440138624380702470L;
    private byte[] content;
    private String type;

    public FileBo(byte[] content, String type) {
        this.content = content;
        this.type = type;
    }

    public FileBo() {
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
