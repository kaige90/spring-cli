package priv.chenkai.spring_cli.client;

import priv.chenkai.spring_cli.client.bo.FileBo;

import java.io.IOException;
import java.util.List;

/**
 * @Author:chenkai
 * @Date: 2018/5/11 10:35
 */
public interface UploadAPI {
    /**
     * 上传单个文件
     * @param file 文件字节流
     * @param contentType 文件类型
     * @param rootPath 文件根目录
     * @return
     * @throws IOException
     */
    String upload(byte[] file,String contentType, String rootPath) throws IOException;

    /**
     * 批量上传文件
     * @param files 文件列表
     * @param rootPath 文件根目录
     * @return
     * @throws IOException
     */
    List<String> uploads(List<FileBo> files, String rootPath) throws IOException;
}
