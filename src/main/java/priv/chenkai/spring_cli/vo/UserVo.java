package priv.chenkai.spring_cli.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * 用户
 *
 * @author chenkai
 * @date 2017/11/21
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserVo implements Serializable{
    private Integer id; //用户ID
    private String name;    //姓名
    private String mobile;  //手机
    private Integer sex;    //性别
    private String nickname;    //昵称
    private Integer status; //状态
    private Integer createTime; //创建时间
    private Integer updateTime; //更新时间

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }
}
