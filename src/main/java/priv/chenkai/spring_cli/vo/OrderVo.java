package priv.chenkai.spring_cli.vo;

import java.io.Serializable;

/**
 * 订单
 */
public class OrderVo implements Serializable{
    private static final long serialVersionUID = -8380709983273890763L;
    public String orderId;  //订单编号
    private Integer price;  //单价
    private Integer num;    //数量
    private String productName; //产品名称
    private Integer cost;       //总价
    private Long uid;   //用户id

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
