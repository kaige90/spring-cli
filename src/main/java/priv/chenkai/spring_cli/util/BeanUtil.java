package priv.chenkai.spring_cli.util;

import org.springframework.beans.BeanUtils;
import priv.chenkai.spring_cli.model.User;
import priv.chenkai.spring_cli.vo.UserVo;

/**
 * Created by chenkai on 2018/1/8.
 */
public class BeanUtil {
    public static UserVo toVo(User user){
        UserVo vo = new UserVo();
        if(user == null){
            return null;
        }
        BeanUtils.copyProperties(user,vo);
        return vo;
    }

    public static User toPo(UserVo vo){
        User po = new User();
        if(vo == null){
            return null;
        }
        BeanUtils.copyProperties(vo,po);
        return po;
    }
}
