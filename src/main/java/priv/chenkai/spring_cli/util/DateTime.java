package priv.chenkai.spring_cli.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期、时间常用方法类.
 * <p>
 * 
 */
public final class DateTime {

	
	private DateTime() {

	}
	
	/**
	 * 获取当前时间，以秒返回
	 * @return
	 */
	public static int getCurrTime(){
		return (int)(System.currentTimeMillis()/1000);
	}

	/**
	 * 当前是哪一天
	 * @return
	 */
	public static int getDayCount() {
		long daynum = System.currentTimeMillis() / 1000 / 60 / 60 / 24;

		return (int) daynum;
	}

	/**
	 * 当前是哪一天
	 * @param datetime
	 * @return
	 */
	public static int getDayCount(final String datetime) {
		int[] arr = parseDatetimeToArray(datetime);
		int year = arr[0];
		int month = arr[1];
		int day = arr[2];

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day);
		long daynum = cal.getTimeInMillis() / 1000 / 60 / 60 / 24;
		return (int) daynum;
	}

	public static int getDayCount(final String date1, String date2) {

		int dayCount1 = DateTime.getDayCount(date1);
		int dayCount2 = DateTime.getDayCount(date2);
		return (dayCount1 - dayCount2);
	}

	public static String getDate() {
		return getDate(System.currentTimeMillis());
	}

	public static String getDate(final int daynum) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, daynum);
		return getDate(cal.getTimeInMillis());
	}

	public static String addDate(final int daynum) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, daynum);
		return getDate(cal.getTimeInMillis());
	}

	public static String addDate(final String date, final int daynum) {
		int[] arr = parseDatetimeToArray(date);
		int year = arr[0];
		int month = arr[1];
		int day = arr[2];

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day + daynum);
		return getDate(cal.getTimeInMillis());
	}

	/**
	 * 将时间或者日期转换成int数组
	 * 
	 * @param datetime
	 * @return
	 */
	public static int[] parseDatetimeToArray(final String datetime) {
		int year = Integer.parseInt(datetime.substring(0, 4));
		int month = Integer.parseInt(datetime.substring(5, 7));
		int day = Integer.parseInt(datetime.substring(8, 10));

		return new int[] { year, month, day };
	}

	private static final SimpleDateFormat GET_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat GET_DATE_FORMAT_2 = new SimpleDateFormat("yyyyMMdd");

	public static synchronized String getDate(final long millis) {
		Date date = new Date();
		if (millis > 0) {
			date.setTime(millis);
		}
		return GET_DATE_FORMAT.format(date);
	}
	
	public static synchronized String getDate2(final long millis) {
		Date date = new Date();
		if (millis > 0) {
			date.setTime(millis);
		}
		return GET_DATE_FORMAT_2.format(date);
	}
	
	/**
	 * 返回long时间戳
	 * @param dateTime 格式yyyy-MM-dd
	 * @return
	 */
	public static long getDateLong(String dateTime) {
		Date date;
		try {
			date = GET_DATE_FORMAT.parse(dateTime);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * 返回long时间戳
	 * @param dateTime 格式yyyyMMdd
	 * @return
	 */
	public static long getDateLong2(String dateTime) {
		Date date;
		try {
			date = GET_DATE_FORMAT_2.parse(dateTime);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static int getHour() {
		Calendar cld = Calendar.getInstance();
		return cld.get(Calendar.HOUR_OF_DAY);
	}

	public static int getDay() {
		Calendar cld = Calendar.getInstance();
		return cld.get(Calendar.DAY_OF_MONTH);
	}

	public static int getMonth() {
		Calendar cld = Calendar.getInstance();
		return cld.get(Calendar.MONTH);
	}

	public static int getMinute() {
		Calendar cld = Calendar.getInstance();
		return cld.get(Calendar.MINUTE);
	}

	public static String getTime() {
		return getTime(0);
	}

	/**
	 * long millis加上int minute 如int很大会出现被载断情况，得出的结果错误， 现将int转换成long后再进行计算.
	 * 如计算一年后的时间24*60*365*60*1000此数已超出int范围,截断后计算出错。
	 */
	public static String addTime(final int minute) {
		long millis = System.currentTimeMillis();
		millis = millis + (minute * 60L * 1000L);
		return getTime(millis);
	}

	public static String addTime(String time, int minute) {
		long millis = getTimestamp(time);
		millis = millis + (minute * 60L * 1000L);
		return getTime(millis);
	}

	private static final SimpleDateFormat GET_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 返回时间long 
	 * @param dateTime 格式yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static long getDateTimeLong(String dateTime){
		Date date;
		try {
			date = GET_TIME_FORMAT.parse(dateTime);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static String getTime(int second) {
		long millis = (long) second * 1000L;
		return getTime(millis);
	}
	
	public static String getTime2(int second) {
		long millis = (long) second * 1000L;
		return getTime2(millis);
	}

	public static String getTime(String time) {
		if (time == null) {
			return null;
		}
		else {
			return time.substring(0, 19);
		}
	}

	public static synchronized String getTime(final long millis) {
		Date date = new Date();
		if (millis > 0) {
			date.setTime(millis);
		}
		return GET_TIME_FORMAT.format(date);
	}
	
	public static synchronized String getTime2(final long millis) {
		Date date = new Date();
		if (millis > 0) {
			date.setTime(millis);
		}
		return GET_DATE_FORMAT.format(date);
	}

	/**
	 * 根据字符串获取时间戳
	 * 
	 * TODO old 根据字符串获取时间戳方法很消耗性能，待优化
	 * 
	 * @param datetime
	 * @return
	 */
	public static long getTimestamp(final String datetime) {
		Calendar cal = Calendar.getInstance();

		int year = Integer.parseInt(datetime.substring(0, 4));
		int month = Integer.parseInt(datetime.substring(5, 7));
		int day = Integer.parseInt(datetime.substring(8, 10));

		int hour = Integer.parseInt(datetime.substring(11, 13));
		int minute = Integer.parseInt(datetime.substring(14, 16));
		int second = Integer.parseInt(datetime.substring(17, 19));

		cal.set(year, month - 1, day, hour, minute, second);
		if (datetime.length() > 19) {
			int mill = Integer.parseInt(datetime.substring(20));
			cal.set(Calendar.MILLISECOND, mill);
		}
		else {
			cal.set(Calendar.MILLISECOND, 0);
		}

		return cal.getTimeInMillis();
	}

	public static long getTimestamp() {
		Calendar cal = Calendar.getInstance();
		return cal.getTimeInMillis();
	}

	public static int getUnixTimestamp() {
		long timestamp = getTimestamp();
		return (int) (timestamp / 1000);
	}

	public static int getUnixTimestamp(String datetime) {
		long timestamp = getTimestamp(datetime);
		return (int) (timestamp / 1000);
	}

	private static final String IS_DATE_REGEX = "^[0-9]{4}\\-[0-9]{1,2}\\-[0-9]{1,2}$";

	/**
	 * 判断字符串是否为正确的日期格式
	 * 
	 * @param str
	 * @return 是否合法日期格式
	 */
	public static boolean isDate(final String date) {
		if (date == null) {
			return false;
		}

		return date.matches(IS_DATE_REGEX);
	}

	private static final String IS_TIME_REGEX = "^[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}$";

	/**
	 * 判断字符串是否为正确的时间格式
	 * 
	 * @param str
	 * @return 是否合法时间格式
	 */
	public static boolean isTime(final String time) {
		if (time == null) {
			return false;
		}
		return time.matches(IS_TIME_REGEX);
	}

	private static final String IS_DATETIME_REGEX = "^[0-9]{4}\\-[0-9]{1,2}\\-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}(\\.[0-9]{1,3})?$";

	/**
	 * 判断字符串是否为正确的日期 + 时间格式
	 * 
	 * @param str
	 * @return 是否合法日期 + 时间格式
	 */
	public static boolean isDateTime(final String str) {
		if (str == null || str.length() == 0) {
			return false;
		}
		return str.matches(IS_DATETIME_REGEX);
	}

	public static int getSecond(final String datetime) {
		long time = getTimestamp(datetime);
		return (int) (time / 1000);
	}

	@SuppressWarnings("deprecation")
	public static String getGMT(final String time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(getTimestamp(time));
		Date date = cal.getTime();
		return date.toGMTString();
	}

	private static final String[] CN_WEEK_NAMES = { "天", "一", "二", "三", "四", "五", "六" };

	public static String getWeekName() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_WEEK) - 1;
		return CN_WEEK_NAMES[day];
	}

	private static final String[] EN_WEEK_NAMES = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

	public static String getWeekName(final String datetime) {
		Calendar cld = Calendar.getInstance();
		cld.setTimeInMillis(getTimestamp(datetime));
		int num = cld.get(Calendar.DAY_OF_WEEK) - 1;
		return EN_WEEK_NAMES[num];
	}

	/**
	 * 获取月份的天数
	 * 
	 * @param monthNum
	 *            0:表示当前月份 负数：表示前n个月份 整数：表示后n个月份
	 * @return
	 */
	public static int getDayCountOfMonth(final int monthNum) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, monthNum);
		cal.set(Calendar.DATE, 1);
		int daynum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return daynum;
	}

	/**
	 * 获取月份的第一天
	 * 
	 * @param monthNum
	 *            0:表示当前月份 负数：表示前n个月份 整数：表示后n个月份
	 * @return
	 */
	public static String getFirstDayOfMonth(final int monthNum) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, monthNum);
		cal.set(Calendar.DATE, 1);
		return getDate(cal.getTimeInMillis());
	}

	public static String getFirstDayOfMonth(final String date, final int monthNum) {
		int[] arr = parseDatetimeToArray(date);
		int year = arr[0];
		int month = arr[1];
		int day = arr[2];

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day);
		cal.add(Calendar.MONTH, monthNum);
		cal.set(Calendar.DATE, 1);
		return getDate(cal.getTimeInMillis());
	}

	/**
	 * 获取星期一
	 * 
	 * @param date
	 * @return
	 */
	public static String getMonday(final String date) {
		int[] arr = parseDatetimeToArray(date);
		int year = arr[0];
		int month = arr[1];
		int day = arr[2];

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day);

		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, 2);
		return getDate(cal.getTimeInMillis());
	}

	/**
	 * 判断传入的日期是否为今天
	 * 
	 * @param time
	 *            String
	 * @return boolean
	 */
	public static boolean isToday(String time) {
		if (time == null || time.length() < 10) {
			return false;
		}

		time = time.substring(0, 10);
		if (DateTime.getDate().equals(time)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 判断是否是今天
	 * @param time 时间的int值秒级别
	 * @return
	 */
	public static boolean isToday(int time){
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DATE),0,0,0);
		
		if(cal.getTimeInMillis()/1000 < time){
			return true;
		}
		return false;
	}

	private static final SimpleDateFormat GET_INT_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static synchronized String getIntTime() {
		Date date = new Date();
		return GET_INT_TIME_FORMAT.format(date);
	}
	
	public static String getBefore(long time){
		String message = "";
		long now = System.currentTimeMillis();
		if(now>time){
			//计算是不是1天内的时间差
			long diffTime = now - time;
			if(diffTime < 86400000){//一天内
				//是1天内的时间差
				int diff = (int)diffTime/3600000;
				if(diff>0){
					return diff + "小时前";
				}else{
					diff = (int)diffTime/60000;
					if(diff>0){
						return diff + "分钟前";
					}else{
						if(diffTime>=5000){
							return diffTime/1000 + "秒前";
						}else{
							return "刚刚";
						}
					}
				}
			}else if(diffTime<86400000L * 30){//月内
				return diffTime/86400000L + "天前";
			}else if(diffTime<86400000L * 365){//年内
				return diffTime/(86400000L * 30) + "月前";
			}else {
				return diffTime/(86400000L * 365) + "年前";
			}
			
			/*
			if(diffTime<=86400000){
				//是1天内的时间差
				int diff = (int)diffTime/3600000;
				if(diff>0){
					message = diff + "小时前";
				}else{
					diff = (int)diffTime/60000;
					if(diff>0){
						message = diff + "分钟前";
					}else{
						if(diffTime>=5000){
							message = diffTime/1000 + "秒前";
						}else{
							message = "刚刚";
						}
					}
				}
			}else{
				Calendar c1 = Calendar.getInstance();
				Calendar c2 = Calendar.getInstance();
				
				c1.setTimeInMillis(now);
				c2.setTimeInMillis(time);
				
				int YearDiff = c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
				int MonthDiff = c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
				int DayDiff = c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH) ;
				if(MonthDiff < 0){
					MonthDiff += 12;
					YearDiff--;
				}
				if(YearDiff>0){
					message = YearDiff + "年前";
				}else{
					if(MonthDiff>0){
						message = MonthDiff + "个月前";
					}else{
						DayDiff = c1.get(Calendar.DAY_OF_MONTH)  - c2.get(Calendar.DAY_OF_MONTH) ;
						if(DayDiff>0){
							message = DayDiff + "天前";
						}
					}
				}
			}
			*/
		}
		return message;
	}

	public static void main(final String[] args) {
		 long flag = DateTime.getUnixTimestamp();
		 // Systemout.println(flag);
//		int t = DateTime.getCurrTime()-60*60*24*360;
//		int t = 1430816842;
//		int t=1420041600;
//		System.out.println(DateTime.getDate(t*1000L));
//		System.out.println(DateTime.getBefore(t*1000L));
//		System.out.println(DateTime.getTimestamp("2015-07-01 00:00:00")/1000);
//		System.out.println(DateTime.getTimestamp("2015-08-01 00:00:00")/1000);
		System.out.print(DateTime.getTime2(DateTime.getCurrTime()));
		
	}

}
